
from AppController import controller
# from reset import appReset


class main(controller):
    def __init__(self):
        super().__init__()

    def Run(self, all_tasks, descriptions):
        # print(descriptions)
        print('**************************************************')
        print('*    Please 1 to add a new task                  *')
        print('*    Please 2 to List task                       *')
        print('*    Please 3 to edit a task                     *')
        print('*    Please 4 to delete a task                   *')
        print('*    Please 5 to quit the application            *')
        print('**************************************************')
        operation = input("\nPlease Enter Key: ")
        print('_________________________________________________')

        if int(operation) == 1:
            self.Create(all_tasks, descriptions)
            self.Run(all_tasks, descriptions)
        elif int(operation) == 3:
            self.Edit(all_tasks, descriptions)
            self.Run(all_tasks, descriptions)
        elif int(operation) == 4:
            self.Delete(all_tasks, descriptions)
            self.Run(all_tasks, descriptions)
        elif int(operation) == 2:
            self.Display(all_tasks, descriptions)
            self.Run(all_tasks, descriptions)
        elif int(operation) == 5:
            print('quit an application')
            return
        else:
            print("Please Enter Key '1' or '2' or '3' or '4' and 'Q' ")
            self.Run(all_tasks, descriptions)


app = main()
if __name__ == '__main__':
    app.Run([], {})
