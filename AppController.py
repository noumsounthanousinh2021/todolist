# from reset import appReset


class controller():
    def __init__(self):
        super()
        self.all_tasks = ""
        self.descriptions = ""

    def Create(self, all_tasks, descriptions):
        new_task = input('Enter Title: ')
        all_tasks.append(new_task)
        des = self.addDescription(new_task, descriptions)
        print('___________Added Item Successfully!____________')
        if (des == 'false'):
            descriptions[new_task] = 'No detail'
            # self.Display(all_tasks, descriptions)
        else:
            pass
            # self.Display(all_tasks, descriptions)

    def addDescription(self, task, descriptions):
        description = input('Enter Description: ')
        # print('_________________________________________________')
        if (description != ''):
            descriptions[task] = description
            return descriptions
        else:
            return 'false'

    def Display(self, all_tasks, descriptions):
        print('\nYour Tasks: ')

        if len(all_tasks) <= 0:
            print('Not Found Tasks!')
        else:
            for index, task in enumerate(all_tasks):
                print('_________________________________________________')
                print(f'{index+1}: {task}       | {descriptions[task]}')
            print('_________________________________________________')
            print('')

    def Delete(self, all_tasks, descriptions):
        task_number = self.Validation(all_tasks, 'remove')

        del descriptions[all_tasks[task_number-1]]
        all_tasks.remove(all_tasks[task_number-1])

        print(f'\nItem {task_number} removed!')

        # self.Display(all_tasks, descriptions)

    def Edit(self, all_tasks, descriptions):
        task_number = self.Validation(all_tasks, 'edit')

        n = (int(task_number)-1)
        detail = descriptions[all_tasks[n]]

        print('---------- Edit Data--------')
        new_task = input('Enter Title: ')
        all_tasks[task_number-1] = new_task
        self.addDescription(new_task, descriptions)
        print(f'Item {task_number} edited!')
        # descriptions[new_task] = des
        descriptions[new_task] = detail
        # self.Display(all_tasks, descriptions)

    def Validation(self, all_tasks, operation):
        task_number = input(
            f'Enter the number of the task you want to {operation}: ')

        valid = False
        while not valid:
            try:
                number = int(task_number)
                valid = True
            except:
                task_number = input('Please provide a valid task number: ')

        if not (0 < number <= len(all_tasks)):
            # print('Task not found!')
            # self.Validation(all_tasks, operation)
            print('Close application')
            return
        else:
            return number

    def Reset(self, all_tasks, descriptions):
        # print(descriptions)
        self.Display(all_tasks, descriptions)
