const yargs = require("yargs");
const appControllers = require('./class/controller');
const { log } = require("console");


// yargs.command({
//     command: "helps",
//     describe: "For help command",
//     builder: {
//         title: {
//             describe: "task title",
//             type: "string",
//             demandOption: false,
//         },
//     },
//     handler: function (argv) {
//         const res = new appControllers().help()
//     },
// });


yargs.command({
    command: "add",
    describe: "Add a new task | cmd: node app add --title=\"test\" --des=\"test\"",
    builder: {
        title: {
            describe: "task title",
            type: "string",
            demandOption: true,
        },
        des: {
            describe: "task description",
            type: "string",
            demandOption: true,
        },
    },
    handler: function (argv) {
        const rnd = Math.floor((Math.random() * 10000000) + 1).toString()
        const res = new appControllers(rnd, argv.title, argv.des).create()
    },
});

yargs.command({
    command: "list",
    describe: "Get all task | cmd: node app list",
    handler: function () {
        const res = new appControllers().all()
    },
});

yargs.command({
    command: "filter",
    describe: "Get a specific task with the title | cmd: node app filter --title=\"test\"",
    builder: {
        title: {
            describe: "task title",
            type: "string",
            demandOption: true,
        }
    },
    handler: function (argv) {
        const res = new appControllers().filter(argv.title)
    },
});

yargs.command({
    command: "update",
    describe: "Update a specific task with the id | cmd: node app update --id=\"number\" --title=\"test\" --des=\"test\"",
    builder: {
        id: {
            describe: "task Id",
            type: "string",
            demandOption: true,
        },
        title: {
            describe: "task title",
            type: "string",
            demandOption: false,
        },
        des: {
            describe: "task description",
            type: "string",
            demandOption: false,
        }
    },
    handler: function (argv) {
        const res = new appControllers(argv.id, argv.title, argv.todo).update()
    },
});

yargs.command({
    command: "delete",
    describe: "Remove a specific task with the id | cmd: node app delete --id=\"number\"",
    builder: {
        id: {
            describe: "task title",
            type: "string",
            demandOption: true,
        }
    },
    handler: function (argv) {
        const res = new appControllers(argv.id).delete()
    },
});


yargs.parse();