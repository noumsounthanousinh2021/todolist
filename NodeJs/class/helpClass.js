class helpClass {

    constructor(title) {
        this.title = title
    }

    _help() {

        console.log(`command insert task/n` +
            `- cmd: $ node app add --title="Write Title as you want" --des="Write everything as you want"/n/n` +

            `command update task/n` +
            `- cmd: $ node app update --id="using ID for update" --title="Write Title as you need to change" --des="Write everything as you need to change"/n` +
            `explance:/n` +
            `If you don't write system wont to change/n` +

            `command delete task/n` +
            `- cmd: $ node app delete --id="using ID for delete"/n` +

            `command list all task/n` +
            `- cmd: $ node app list/n` +

            `command filter task by title/n` +
            `- cmd: $ node app filter --title="Write Title as you want to filter"`);
    }



}

module.exports = helpClass